var app        = require('./app'),
    proj       = require('./project'),
    crawler    = require("simplecrawler"),
    uri        = require("urijs"),
    util       = require("./util"),
    async      = require("async"),
    log        = app.logger,
    FetchQueue = require("./node_modules/simplecrawler/lib/queue.js"),
    isOnline   = require('is-online')
;

module.exports = {

    initCrawler: function() {
        /*var this.crawler = new this.crawler("bit.ly");
          this.crawler.initialPath      = "/UHfDGO";*/
        this.cols   = {};
        this.crawler = new crawler("google.com");
        this.crawler.initialPath      = "/";
        this.crawler.initialPort      = 80;
        this.crawler.initialProtocol  = "https";
        this.crawler.interval         = 500;
        this.crawler.maxConcurrency   = 5;
        this.crawler.filterByDomain   = false;
        this.crawler.ignoreInvalidSSL = true;
        this.crawler.acceptCookies    = true;
        this.crawler.userAgent        = '';//'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.99 Safari/537.36';
        this.crawler.timeout          = 20 * 1000;
        this.crawler.listenerTTL      = 10 * 3 * 20 * 1000;
        /*this.crawler.cache = new this.crawler.cache('./cache');*/
        this.crawler.discoverResources = function(buf, item) {
            return [];
        };

        this.crawler.on("fetchcomplete", function(item, buf, res) {

            // if redirect we have to take last fetched url
            // if not redirect fetchedUrls = [ 'http://www.host.com' ] without path and query string
            var fetchedUrl  = res.fetchedUrls.length > 1 ? res.fetchedUrls.shift() : item.url;

            var l = this.cols[item.url];
            // sometimes fetchcomplete fires two times. On the second fire there is no item.url in cols anymore
            if (!l) return;

            var col  = l.col;
            var host = l.h;
            delete this.cols[item.url];

            // check we were not redirected out of the domain
            var fetchedHost = uri(fetchedUrl).hostname();
            if (!util.isSubdomainOf(fetchedHost, host)) {
                return log.warn('fetch.complete ' + item.url, fetchedHost + ' is not subdomain of ' + item.host);
            }

            if (!res.headers['content-type']) {
                res.headers['content-type'] = 'text/html';
                console.log(res.headers);
                console.log(buf.toString('utf-8'));
            }

            var p       = res.headers['content-type'].split(';'),
                mime    = p[0],
                charset = p[1] ? p[1].split('=').pop() : '';

            // set fetched
            // set mime
            var upd = { $set: { s: 'f', m: mime } };


            var isCache = mime === 'text/html' ? true : false;

            // nginx can not find encoded filenames
            // encoded filenames go to cache
            if (!isCache) {
                var url_decoded = uri.decode(item.url);
                if (url_decoded !== item.url) {
                    isCache = true;
                }
            }

            // mark as cache to find for analyzing
            if (isCache) {
                upd.$set.cache = 1;
            }

            log.silly('fetch complete ' + item.url);
            app.db.collection(col).update({ u: item.url }, upd, function(err, updated) {
                if (err) return log.error('update (' + col + ') ' + item.url + ' set fetched err: ' + err);

                var data = {
                    item:       item,
                    h:          res.headers,
                    url:        fetchedUrl,
                    mime:       mime,
                    charset:    charset,
                    http:       res.httpVersion,
                    statusCode: res.statusCode,
                    ho:         host,
                    col:        col
                };

                //application/atom+xml
                //application/rss+xml
                //application/xhtml+xml
                //application/rdf+xml
                //application/javascript
                //application/x-javascript
                //application/ecmascript
                //application/json
                //application/x-web-app-manifest+json
                //application/vnd.ms-fontobject
                //application/x-font-ttf
                //application/xml
                //font/opentype
                //image/svg+xml
                //image/x-icon
                //image/bmp
                //text\/*
                var enc   = mime.match(/text|javascript|json|ecmascript|atom\+xml|rss\+xml|xhtml\+xml|rdf\+xml|application\/xml/) ? 'utf8' : 'binary';
                data.html = buf.toString(enc);
                data.enc  = enc;
                app.tasker.post(isCache ? 'cache' : 'static', JSON.stringify(data), 'utf8');

                var upd = { $inc: { 'l.f' : 1 }, $addToSet: { hosts: item.host } };
                app.db.collection('projects').update({ h: host }, upd, function(err, updated) {
                    if (err) return log.error('update project (' + host + ') ' + item.url + ' set l.f err: ' + err);
                    /*log.info(upd, updated.result);*/
                });
            });
        }.bind(this));

        this.crawler.on("fetch404",          this.linkError.bind(this));
        this.crawler.on("fetch.start",       function(item, opts)     { log.silly('fetch.start ' + (this.crawler.queue.oldestUnfetchedIndex + 1) + '/' + this.crawler.queue.length + ' ' + item.url); }.bind(this));
        this.crawler.on("fetchheaders",      function(item, res)      { });
        this.crawler.on("fetchdataerror",    function(item, res)      { log.error('fetch.dataerror', item, res.statusCode); });
        this.crawler.on("fetch.stopped",     function(item, res)      { log.error('fetch.stopped', item.url, item.stateData.responseLengthReceived + '/' + item.stateData.responseLength); });
        this.crawler.on("fetchredirect",     function(item, url, res) { log.info('redirect: ' + url.protocol + '://' + url.host + url.path); });
        this.crawler.on("fetcherror",        this.linkError.bind(this));//function(item, res)      { log.error('fetch.error', item.url, res.statusCode); });
        this.crawler.on("fetchtimeout",      function(item, timeout)  { log.error('fetch.timeout ' + timeout + ' ' + item.url); });
        this.crawler.on("fetch.clienterror", function(item, err)      {
            var self = this;
            if (err.code === 'ENOTFOUND') {
                isOnline(function(error, online) {
                    if (error) return log.error('fetch.clienterror ' + item.url + ' err: ' + err + ' isOnline err: ' + error);

                    log.error('fetch.clienterror ' + item.url + ' ' + err.message + ' ' + (online ? ' ONLINE' : ' OFFLINE'));

                    if (!online) return;

                    var l = self.cols[item.url];
                    // sometimes fetchcomplete fires two times. On the second fire there is no item.url in cols anymore
                    if (!l) return;

                    var col  = l.col;
                    var host = l.h;
                    delete self.cols[item.url];

                    var upd = { $set: { s: 'f', code: 'ENOTFOUND' } };
                    app.db.collection(col).update({ u: item.url }, upd, function(err, updated) {
                        if (err) return log.error('update (' + col + ') ' + item.url + ' set fetched err: ' + err);
                        console.log('ENOTFOUND', updated);
                    });
                });
            }
            else {
                log.error('fetch.clienterror ' + item.url + ' ' + err);
            }

        }.bind(this));

        this.crawler.on("notmodified",       function(item, res)      { log.info('fetch.notmodified', item.url, res.statusCode); });
        this.crawler.on("crawl.start",       function()               { log.info('crawlstart'); });
        this.crawler.on("discoverycomplete", function(item, res)      { });
        this.crawler.on("complete",          function()               { });
        this.crawler.on("queueadd",          function(item)           { log.info('queue.add ' + item.url); });
        this.crawler.on("queueduplicate",    function(urldata)        { log.error('queue.duplicate', urldata); });
        this.crawler.on("queueerror",        function(err, data)      { log.error('queue.error', err, data); });


        this.crawler.on("queue.drain",       this.drain.bind(this));
        /*this.crawler.on("queue.freeslots",   this.drain.bind(this));*/

        this.crawler.start = this.startCrawl.bind(this);
        this.crawler.crawl = this.crawl.bind(this);
    },

    linkError: function(item, res) {
        log.error('%d\t\t\t\t\t%s', res.statusCode, item.url);
        var fetchedUrl = res.fetchedUrls.shift();

        var l    = this.cols[item.url];
        // sometimes this callback fires two times. On the second fire there is no item.url in cols anymore
        if (!l) return;

        var col  = l.col;
        var host = l.h;
        delete this.cols[item.url];

        app.db.collection(col).update({ u: item.url }, { $set: { s: 'f', code: res.statusCode } }, function(err, updated) {
            if (err) return log.error('update (' + col + ') ' + item.url + ' set fetched err: ' + err);

            var upd = { $inc: {} };
            upd.$inc['l.f'] = 1;
            upd.$inc['l.' + res.statusCode] = 1;
            app.db.collection('projects').update({ h: host }, upd, function(err, updated) {
                if (err) return log.error('update project (' + host + ') ' + item.url + ' set l.f err: ' + err);
            });
        });
    },

    drain: function() {
        var self = this;

        log.silly('queue.drain ' + (this.crawler.queue.oldestUnfetchedIndex + (this.crawler.queue.length > 0 ? 1 : 0)) + '/' + this.crawler.queue.length + ' ' + (this.draining ? 'draining' : 'not draining'));

        if (self.draining)
            return;

        self.draining = true;
        self.crawler.queue = new FetchQueue();

        self.processProjects(function(err) {
            if (err) log.error(err);

            self.draining = false;
        });
    },

    processProjects: function(cb) {
        var self = this;
        async.waterfall([
            function(cb)           { app.db.collection('projects').find({ s: 'r' }).toArray(cb); },
            function(projects, cb) {
                async.each(
                    projects,
                    function(project, cb) {
                        log.silly('project ' + project.url + ' col: ' + project.col);

                        self.processProject(project, cb);
                    },
                    cb
                );
            }
        ], cb);
    },

    processProject: function(project, cb) {
        var self = this;

        async.waterfall([
            function(cb)          { proj.generateColHost(project, cb); },
            function(project, cb) { proj.ensureIndex(project, cb); },
            function(project, cb) {
                app.db.collection(project.col).find({ s: 'n' }, { limit: 100 }).toArray(function(err, links) {
                    if (err) return cb('project ' + project.url + ' err: ' + err);

                    cb(null, project, links);
                });
            },
            function(project, links, cb) {
                if (!links || !links.length) {
                    app.db.collection(project.col).find({ }, { limit: 1 }).toArray(function(err, link) {
                        if (err) return cb('project ' + project.url + ' err: ' + err);

                        if (!link || !link.length) {
                            proj.addStartUrl(project, cb);
                        }
                        else {
                            proj.finish(project, cb);
                        }
                    });
                }
                else {
                    log.silly('send ' + links.length + ' to fetch');
                    self.sendLinksToFetch(project, links, cb);
                }
            }

        ], cb);


    },


    sendLinksToFetch: function(project, links, cb) {
        async.each(
            links,
            function(link, cb) {
                app.tasker.post(
                    'fetch',
                    JSON.stringify({
                        l:   link,
                        h:   project.h,
                        col: project.col,
                    }),
                    'utf8'
                );
                cb();
            }, cb
        );

    },

    startCrawl: function() {

        this.crawler.crawlIntervalID = setInterval(
            function() {
                this.crawler.crawl(this.crawler);
            }.bind(this),
            this.crawler.interval
        );

        this.crawler.emit("crawl.start");
        this.crawler.running = true;

        // Now kick off the initial crawl
        process.nextTick(function() {
            this.crawler.crawl();
        }.bind(this));

        return crawler;
    },


    crawl: function() {
        if (this.crawler._openRequests > this.crawler.maxConcurrency) {
            log.silly('opened requests ' + this.crawler._openRequests, 'listeners ' + this.crawler._openListeners, 'concur ' + this.crawler.maxConcurrency);
            return [];
        }

        this.crawler.queue.oldestUnfetchedItem(function(err, queueItem) {    // eslint-disable-line
            //if (err) log.error('fetch.crawl.oldestUnfetchedItem err: ' + err, 'opened requests ' + this.crawler._openRequests, 'listeners ' + this.crawler._openListeners, 'concur ' + this.crawler.maxConcurrency);

            if (queueItem) {
                log.silly('crawl.queueItem', 'opened requests ' + this.crawler._openRequests, 'listeners ' + this.crawler._openListeners, 'concur ' + this.crawler.maxConcurrency);
                this.crawler.fetchQueueItem(queueItem);

            }

            else if (!this.crawler._openRequests && !this.crawler._openListeners) {
                this.crawler.emit("queue.drain");
            }

            /*else if (this.crawler._openRequests < this.crawler.maxConcurrency) {
              this.crawler.emit("queue.freeslots");
              }*/

            else {
                //log.info(this.crawler.queue);
                this.crawler.emit("queue.drain");
            }
        }.bind(this));

        return this.crawler;
    },

    init: function() {

        this.initCrawler();
        this.crawler.start();

        var worker = app.context.socket('WORKER', { persistent: true, prefetch: this.crawler.maxConcurrency });
        worker.on('data', function(task) {
            task = JSON.parse(task);

            /*console.log('fetch ' + task);*/
            var url = uri.parse(task.l.u);
            url.port = url.port || 80;

            this.cols[task.l.u] = { h: task.h , col: task.col };

            this.crawler.queue.add(
                url.protocol,
                url.hostname,
                url.port,
                url.path + (url.query ? '?' + url.query : ''),
                function(err) {
                    if (err) {
                        if (err.code === 'DUP') {
                            return worker.discard();
                        }

                        log.error('fetch: ' + err + ', requeue');
                        return worker.requeue();
                    }

                    worker.ack();
                });
        }.bind(this));

        worker.connect('fetch', function() {
            log.verbose('fetch worker connected');
        });


    }
};
