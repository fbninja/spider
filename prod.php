<?php

require_once(__DIR__ . '/db.class.php');
require_once(__DIR__ . '/mysql.class.php');
require_once(__DIR__ . '/Config.class.php');
require_once(__DIR__ . '/Prod.class.php');
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Parser
{
    private static $parsers = [];

    public static function factory($ho)
    {
        try {
            if (empty(self::$parsers[$ho])) {
                $cfg = Config::load($ho);
                self::$parsers[$ho] = new Prod();
                self::$parsers[$ho]->cfg($cfg);
            }
            return self::$parsers[$ho];
        }
        catch (Exception $e) {
            echo "createParser: " . $e->getMessage() . "\n";
            die();
        }
    }
}


class P {
    public function __construct() {
        $connection    = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
        $this->channel = $connection->channel();
        $q = $this->channel->queue_declare('prods', false, true, false, false);
/*print_r($q);
die();*/

        /*
        require_once __DIR__ . '/micrometa/src/Jkphl/Micrometa.php';
        $micrometaParser        = new \Jkphl\Micrometa($url);
        $data    = $micrometaParser->toJSON();
        print_r(json_decode($data, true));
        print_r($data);*/



        $cfg = array('mongos' => '127.0.0.1:27017');
        $this->db = db::instance($cfg)->getDB();
        $r = $this->db->spider->projects->find();
        foreach ($r as $v)
            print_r($v);
        $this->mysql = new mysql('spider', 'localhost', 'spider', 'spider', 'utf-8');
        /*$this->config = new Config();*/
    }

    public function callback($msg) {
        $task = json_decode($msg->body, true);
        echo $task['_id'] . ' ' . $task['u'];

        $hash = md5($task['u']);
        $p    = $hash[0] . '/' . $hash[0] . $hash[1] . '/';
        $dir  = __DIR__ . '/data/' . $task['ho'] . '/cache/' . $p;
        $file = $dir . $hash . '.cache';

        if (file_exists($file)) {
            $data = file_get_contents($file);

            list($time, $url, $headers, $format, $c) = explode("\r\n\r\n", $data);

            if ($format === 'gzip')
                $c = gzdecode($c);

            $parser = Parser::factory($task['ho']);

            //$this->createParser($task['ho']);
            $this->createMysqlTable($task['ho']);

            $isAllFields = true;


            if ($parser->checkIsProduct($url, $content)) {
                $item = $parser->parse($c);
                $parser->save($task['u'], $item, $task['ho']);

                /*$item['uri'] = preg_replace("/https?:\/\/[^\/]+/is", '', $task['u']);

                if (!empty($item['imgs'])) $item['imgs'] = json_encode($item['imgs']);
                if (!empty($item['cat']))  $item['cat'] = json_encode($item['cat']);
                if (!empty($item['spec'])) $item['spec'] = json_encode($item['spec']);*/
                foreach ($item as $f => $v) {
                    if (empty($v)) {
                        $isAllFields = false;
                        break;
                    }
                }

                /*$sql = "SELECT id FROM `" . $task['ho'] . "` WHERE `uri` = '" . mysql_real_escape_string($item['uri']) . "'";
                $exists = $this->mysql->selectRow($sql);
                if (empty($exists)) {
                    $sql = "INSERT IGNORE INTO `" . $task['ho'] . "` " . $this->mysql->make_inserts_values($item);
                    if(!$this->mysql->query($sql))
                        die();
                }
                else {
                    $sql = "UPDATE `" . $task['ho'] . "` " . $this->mysql->make_updates_values($item) . " WHERE id=" . $exists['id'];
                    $this->mysql->query($sql);
                }*/
            }
            $upd = array('$set' => array('s' => 'a'));

            // mark not all fields are parsed
            if (!$isAllFields) {
                $upd['$set']['prod'] = 'fld';
            }
            else {
                $upd['$unset']['prod'] = 1;
            }
            $this->db->spider->{$task['col']}->updateOne(array('_id' => new MongoDB\BSON\ObjectId((string)$task['_id'])), $upd/*, array('writeConcern' => array('w' => 1, 'j' => true))*/);
        }
        else echo "file " . $file . " not found\n";

        echo "\n";
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        /*$this->send();*/
    }

    public function run() {
        global $argv;
        $this->channel->basic_qos(null, 1, null);
        $this->channel->basic_consume('prods', '', false, false, false, false, array($this, 'callback'));

        //$this->send();
        $this->wait();
    }

    /*public function send() {
        $projs = $this->db->spider->projects->find();
        $projs = iterator_to_array($projs);

        if (!empty($projs)) {
            foreach ($projs as $proj) {
                try {
                    $cfg = $this->config->load($proj['h']);

                    echo $proj['h'] . " has config\n";

                    $items = $this->db->spider->{$proj['col']}->find(array('s' => 'p', 'm' => 'text/html'));
                    $items = iterator_to_array($items);
                    echo $proj['h'] . " emit " . count($items) . "\n";
                    if (!empty($items)) {
                        foreach ($items as $item) {
                            $item['_id'] = (string)$item['_id'];
                            $item['ho']  = $proj['h'];
                            $item['col'] = $proj['col'];

                            $data = json_encode($item);
                            $msg = new AMQPMessage($data,
                                array('delivery_mode' => 2) # make message persistent
                            );

                            $this->channel->basic_publish($msg, '', 'prods');

                        }
                    }
                }
                catch (Exception $e) {
                    echo $proj['h'] . ': ' . $e->getMessage() . "\n";
                }
            }
        }
    }*/


    /*public function createParser($ho) {
        if (empty($this->parsers[$ho])) {
            try {
                $cfg = $this->config->load($ho);
                $this->parsers[$ho] = new Prod();
                $this->parsers[$ho]->cfg($cfg);
            }
            catch (Exception $e) {
                echo "createParser: " . $e->getMessage() . "\n";
            }
        }
    }*/

    public function createMysqlTable($ho) {
die('create table');
        if (empty($this->tables[$ho])) {
            $tables = $this->mysql->sql2array("SHOW TABLES LIKE '" . $ho . "'");
            if (empty($tables)) {
                $sql = "
                    CREATE TABLE IF NOT EXISTS `" . $ho . "` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `title` varchar(255) NOT NULL,
                        `uri` varchar(255) NOT NULL,
                        `image` varchar(255) NOT NULL,
                        `imgs` text NOT NULL,
                        `brand` varchar(255) NOT NULL,
                        `mpn` varchar(255) NOT NULL,
                        `upc` varchar(255) NOT NULL,
                        `sku` varchar(255) NOT NULL,
                        `price` float(8,2) NOT NULL,
                        `desc` text NOT NULL,
                        `desc_full` text NOT NULL,
                        `spec` text NOT NULL,
                        `cat` text NOT NULL,
                        PRIMARY KEY (`id`)
                    ) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
                $r = $this->mysql->query($sql);
                if ($r) {
                    $this->mysql->query('ALTER TABLE `' . $ho . '` ADD UNIQUE(`uri`);');
                    $this->tables[$ho] = true;
                }
            }
        }
    }

    public function wait() {
        echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

        while(count($this->channel->callbacks)) {
            $this->channel->wait();
        }
    }

    }

    $p = new P();
    $p->run();

