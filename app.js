var rabbit  = require('rabbit.js'),
    mongo   = require('mongoskin'),
    async   = require('async'),
    uri=require('urijs');
    winston = require('winston')
;

module.exports = {

    run: function() {
        this.connect();
    },

    connect: function() {
        this.context = rabbit.createContext('amqp://localhost');
        this.context.on('ready', this.onContextReady.bind(this));

        this.db = mongo.db("mongodb://localhost:27017/spider", {
        });

        /*var config = require('winston/lib/winston/config');*/
        this.logger = new (winston.Logger)({
            transports: [
                new (winston.transports.Console)({
                    level:       'silly',
                    colorize:    'all',
                    showLevel:   false,
                    prettyPrint: true,
                    //align: true,
                    /*formatter: function(options) {
                        // Return string will be passed to logger.
                        return winston.config.colorize(options.level, (undefined !== options.message ? options.message : '') +
                            (options.meta && Object.keys(options.meta).length ? '\n\t'+ JSON.stringify(options.meta) : '' ));
                    }*/
                }),
            ]
        });
        //this.logger.setLevels(winston.config.cli.levels);
        /*this.db.bind('spider');*/
    },

    onContextReady: function() {
        this.logger.verbose('rabbit context ready');
        this.tasker  = this.context.socket('TASK', { persistent: true });
        this.tasker.on('close', function() {
            console.log('tasker closed');
        });

        this.connectToWorkers(function(err) {
            if (err) return console.log('connect to workers err: ' + err);

            this.initWorkers();
        }.bind(this));
    },

    connectToWorkers: function(cb) {
        this.workers = [ 'cache', 'static', 'parse', 'fetch', 'prods' ];
        async.each(
            this.workers,
            function(worker, cb) {
                this.tasker.connect(worker, function() {
                    this.logger.verbose('tasker connected to ' + worker);
                    cb();
                }.bind(this));
            }.bind(this),
            cb
        );
    },

    initWorkers: function() {
        var self = this;
        async.each(
            self.workers,
            function(worker, cb) {
                self.logger.verbose('initializing worker ' + worker);
                try {
                require('./' + worker).init();
                }
                catch (e) {
                }
                cb();
            },
            function(err) {
                if (err) return self.logger.error('initializing workers err: ' + err);

                self.logger.verbose('workers initialized successfully');
            }
        );
    }
};

