var fs      = require("file-system")
;

module.exports = {
    isSubdomainOf: function (subdomain, host) {

        // Comparisons must be case-insensitive
        subdomain   = subdomain.toLowerCase();
        host        = host.toLowerCase();

        // If we're ignoring www, remove it from both
        // (if www is the first domain component...)
        //if (crawler.ignoreWWWDomain) {
        subdomain = subdomain.replace(/^www./ig, "");
        host      = host.replace(/^www./ig, "");
        //}

        // They should be the same flipped around!
        return subdomain.split("").reverse().join("").substr(0, host.length) === host.split("").reverse().join("");
    },

    mkdir: function(dir, cb) {
        fs.exists(dir, function(exists) {
            if (exists) return cb(null);

            fs.mkdir(dir, 0755, cb);
        });
    },

    write: function(file, data, enc, cb) {
        // if enc is binary all utf8 chars will be cracked
        fs.writeFile(file, data, { encoding: enc }, function(err) {
            cb(err, data.length);
        });
    },

    isFile: function(file) {
        try {
            var s = fs.lstatSync(file);

            return s.isFile && !s.isDirectory();
        }
        catch (err) {
            return false;
        }
    }

};
