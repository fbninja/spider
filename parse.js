var app   = require('./app'),
    fs    = require('fs'),
    zlib  = require('zlib'),
    uri   = require("urijs"),
    async = require('async'),
    util  = require('./util'),
    log   = app.logger
;

module.exports = {
    extract_tags: ["href", "src", "url", "location", "codebase", "background", "data", "profile", "action", "open"],
    /*

                  // Now, if agressive_mode is set to true, we look for some
                  // other things
                  $pregs = array();
                  if ($this->aggressive_search == true)
                  {
                  // Links like "...:url("animage.gif")..."
                  $pregs[]="/[\s\.:;](?:".$tag_regex_part.")\s*\(\s*([\"|']{0,1})([^\"'\) ]{1,500})['\"\)]/ is";

                  // Everything like "...href="bla.html"..." with qoutes
                  $pregs[]="/[\s\.:;](?:".$tag_regex_part.")\s*=\s*([\"|'])(.{0,500}?)\\1/ is";

                  // Everything like "...href=bla.html..." without qoutes
                  $pregs[]="/[\s\.:;](?:".$tag_regex_part.")\s*(=)\s*([^\s\">']{1,500})/ is";
                  }
                  */

    discoverRegex: [
        /*/\s?(?:href|src)\s?=\s?(["']).*?\1/ig,*/
        /*/\s?(?:href|src)\s?=\s?[^"'][^\s>]+/ig,*/
        /*/\s?url\((["']).*?\1\)/ig,*/
        /*/\s?url\([^"'\)].*?\)/ig,*/
        /\s?url\(([^"'\)].*?)\)/ig,

        // This could easily duplicate matches above, e.g. in the case of
        // href="http://example.com"
        /*/http(s)?\:\/\/[^?\s><\'\"]+/ig,*/

        // This might be a bit of a gamble... but get hard-coded
        // strings out of javacript: URLs. They're often popup-image
        // or preview windows, which would otherwise be unavailable to us.
        // Worst case scenario is we make some junky requests.
        /*/^javascript\:[a-z0-9\$\_\.]+\(['"][^'"\s]+/ig,*/

    ],

    // Supported Protocols
    allowedProtocols: [
        /^http(s)?$/i,                  // HTTP & HTTPS
        /^(rss|atom|feed)(\+xml)?$/i    // RSS / XML
    ],

    cfgs: [],

    initRegex: function() {
        var re;
        // 1. <a href="...">LINKTEXT</a> (well formed link with </a> at the end and quotes around the link)
        // Get the link AND the linktext from these tags
        // This has to be done FIRST !!
        re = new RegExp('<\\s*a\\s[^<>]*\\s*(?:' + this.extract_tags.join('|') + ')\\s*=\\s*(?:"([^"]+)"|\'([^\']+)\'|([^\\s><\'"]+))[^<>]*>', 'gi');
        this.discoverRegex.push(re);

        // Second regex (everything that could be a link inside of <>-tags)
        re = new RegExp('<[^<>]*\\s(?:' + this.extract_tags.join('|') + ')\\s*=\\s*(?:"([^"]+)"|\'([^\']+)\'|([^\\s><\'"]+))[^<>]*>', 'gi');
        this.discoverRegex.push(re);

    },

    parse: function(task, cb) {
        var self = this;


        if (!task.col) return cb('parse: no col in task');
        if (!task.ho)  return cb('parse: no ho in task');

        async.waterfall([
            function(cb)              { fs.readFile(task.file, 'binary', cb); },
            function(data, cb)        { self.processFileData(task, data, cb); },
            function(data, cb)        { self.uncompress(data, cb); },
            function(data, cb)        { self.clean(data, cb); },
            function(data, cb)        { self.find(data, cb); },
            function(data, links, cb) { self.filter(task.ho, data, links, cb); },
            function(data, links, cb) { self.save(task, data, links, cb); },

        ], function(err, links, totals) {
            if (err) return cb(task.file + ' err: ' + err);

            // set url parsed (p)
            app.db.collection(task.col).findAndModify({ u: task.item.url, s: 'f' }, {}, { $set: { s: 'p' } }, { new: true }, function(err, modifiedLink) {
                if (err) return cb(err);

                log.silly('parsed ' + task.item.url + ' update set p', modifiedLink.lastErrorObject);

                app.db.collection('projects').update({ h: task.ho }, { $inc: { 'l.n': totals.new, 'l.p': 1 } }, function(err, upd) {
                    log.silly('parsed ' + task.item.url + ', found: ' + totals.new + '/' + links.length + ' links');

                    if (modifiedLink.value) {
                        modifiedLink.value.ho  = task.ho;
                        modifiedLink.value.col = task.col;
                        app.tasker.post('prods', JSON.stringify(modifiedLink.value), 'utf8');
                    }
                    cb(err);

                });
            });
        });
    },

    processFileData: function(task, data, cb) {
        if (!data) return cb('empty file data ' + task.file);
        var parts = data.split("\r\n\r\n");
        data = {
            t      : parts[0],
            url    : parts[1],
            h      : parts[2],
            format : parts[3],
            c      : parts[4],
        };

        /*try {
          data = JSON.parse(data);
          }
          catch (e) {
          console.log(data);
          return cb(e.message);
          }*/

        data.fetchedUrl     = uri.parse(data.url);
        data.fetchedUrl.url = data.url;

        // for collecting mailto
        data.fetchedUrl.ho  = task.ho;


        cb(null, data);
    },

    uncompress: function(data, cb) {
        if (data.format === 'gzip') {
            zlib.gunzip(new Buffer(data.c, 'binary'), function(err, c) {
                data.c = c;
                cb(err, data);
            });
        }
        else cb(null, data);
    },

    clean: function(data, cb) {
        data.c = data.c.toString('utf8');
        data.c = data.c.replace(/<!--([\s\S]+?)-->/g, "");
        data.c = data.c.replace(/<script(.*?)>([\s\S]*?)<\/script>/gi, "");

        cb(null, data);
    },

    find: function(data, cb) {
        var links = this.discoverRegex
        .reduce(function(list, regex) {
            //console.log('regex: ' + regex);
            var lnks = [];
            while (null !== (matches = regex.exec(data.c))) {
                lnks.push(matches[1]);
            }

            return list.concat(this.cleanExpandResources(lnks/*data.c.match(regex)*/, data.fetchedUrl));
        }.bind(this), [])
        .reduce(function(list, check) {
            if (list.indexOf(check) < 0) {
                return list.concat([check]);
            }

            return list;
        }, []);

        cb(null, data, links);
    },

    filter: function(ho, data, links, cb) {
        if (!links) return cb(null, data, links);

        var regexp = '';
        if (this.cfgs[ho] && this.cfgs[ho].skip) {
            regexp = this.cfgs[ho].skip.join('|');
            regexp = new RegExp(regexp, 'i');
        }

        async.filter(
            links,
            function(link, cb) {
                var m = regexp ? !regexp.test(link) : true;
                cb(m);
            },
            function(filtered) {
                cb(null, data, filtered);
            }
        );
    },

    cleanURL: function (page, URL) {
        // url can be undefined
        if (!URL) return;

        return URL.replace(/^(?:\s*href|\s*src)\s*=+\s*/i, "")
            .replace(/^\s*/, "")
            .replace(/^url\((.*)\)/i, "$1")
            /*.replace(/^javascript\:\s*[a-z0-9]+\((.*)\)/i, "$1")*/
            .replace(/^javascript\:\s*[a-z0-9]+\((.*)\);?/i, "")
            .replace(/^(['"])(.*)\1$/, "$2")
            .replace(/^\((.*)\)$/, "$1")
            .replace(/^\/\//, page.protocol + "://")
                .replace(/\&amp;/gi, "&")
                .replace(/\&#38;/gi, "&")
                .replace(/\&#x00026;/gi, "&")
                .split("#")
                .shift()
                .trim();
    },

    cleanExpandResources: function (urlMatch, page) {
        var /*crawler = this,*/
        resources = [];

        if (!urlMatch) {
            return [];
        }

        //log.info('urlMatch', urlMatch);

        return urlMatch
        .map(this.cleanURL.bind(this, page))
        .reduce(function(list, URL) {
            // if url is # it will be empty after cleaning
            // and we do not want it to be absoluted to current page
            if (!URL || !URL.length) {
                return list;
            }

            // Ensure URL is whole and complete
            try {
                //var ur = URL;
                URL = uri.decode(URL);
                //var ur2 = URL;
                URL = uri(URL)
                .absoluteTo(page.url || "")
                .normalize();

                var splitted = page.hostname.split('.');
                var pageDomain = [];
                pageDomain.push(splitted.pop());
                pageDomain.push(splitted.pop());
                pageDomain = pageDomain.reverse().join('.');

                // how facebook and twitter pages come here? Checking page.ho
                // added fetchedHost check in fetch.js, commented hge.ho
                if (!util.isSubdomainOf(URL.hostname(), pageDomain) /* || !util.isSubdomainOf(URL.hostname(), page.ho)*/) {
                    /*console.log('skip: ' + URL.toString() + ' is not subdomain of ' + page.hostname);*/
                    return list;
                }

                URL = URL.toString();
                //console.log(ur + ' => ' + ur2 + ' => ' + URL);
            } catch (e) {
                // But if URI.js couldn't parse it - nobody can!
                if (URL.match(/mailto:/)) {
                    var mail = URL.split(':').pop();
                    app.db.collection('projects').update({ h: page.ho }, { $addToSet: { mails: mail } }, function(err, upd) {
                        if (upd.result.nModified > 0)
                            log.warn('+@mail ' + mail);
                    });
                }
                else if (URL.match(/tel:/)) {
                    var tel = URL.split(':').pop();
                    app.db.collection('projects').update({ h: page.ho }, { $addToSet: { tels: tel } }, function(err, upd) {
                        if (upd.result.nModified > 0)
                            log.warn('+@tel ' + tel);
                    });
                }
                else if (URL.match(/facebook\.com|twitter\.com|pinterest\.com/)) {
                }
                else if (!URL.match(/javascript|data:image/)) {
                    log.error('uri ' + URL + ' err: ' + e.message, page.url);
                }
                return list;
            }

            // If we hit an empty item, don't return it
            if (!URL.length) {
                return list;
            }

            // If we don't support the protocol in question
            if (!this.protocolSupported(URL)) {
                return list;
            }

            // Does the item already exist in the list?
            if (resources.reduce(function(prev, current) {
                return prev || current === URL;
            }, false)) {
                return list;
            }

            return list.concat(URL);
        }.bind(this), []);
    },

    protocolSupported: function(URL) {
        var protocol,
        self = this;

        try {
            protocol = uri(URL).protocol();

            // Unspecified protocol. Assume http
            if (!protocol) {
                protocol = "http";
            }

        } catch (e) {
            // If URIjs died, we definitely /do not/ support the protocol.
            return false;
        }

        return self.allowedProtocols.reduce(function(prev, protocolCheck) {
            return prev || !!protocolCheck.exec(protocol);
        }, false);
    },

    save: function(task, data, links, cb) {
        if (!links) return cb();

        var totals = {
            new: 0,
            old: 0
        };

        var added = [];
        async.each(
            links,
            function(l, cb) {
                if (!l) return cb();

                app.db.collection(task.col).update(
                    { u: l },
                    { $set: { d: new Date() }, $setOnInsert: { u: l, s: 'n', } },
                    { upsert: true },
                    function(err, upd) {
                        if (err) return cb('links insert err: ' + err);

                        if (upd.result.upserted) {
                            totals.new++;
                            added.push(l);
                        }
                        else
                            totals.old++;
                        cb();
                    });
            }.bind(this),
            function(err) {
                if (added.length) {
                    console.log();
                    added.map(function(l) {
                        //log.warn('+\t\t\t\t\t\t\t\t' + l + '\t\t<= ' + data.url[> + ' ' + JSON.stringify(upd.result)<]);
                        log.warn('+\t\t\t\t\t\t\t\t' + l);
                    });
                    console.log();
                }

                cb(err, links, totals);
            });
    },

    loadCfg: function(ho, cb) {
        var self = this;
        if (!this.cfgs[ho]) {
            var runner = require('child_process');

            runner.exec(
                'php -r \'include("/home/sites/mirror/crawler/configs/' + ho + '.php"); print json_encode($cfg);\'',
                function (err, stdout, stderr) {
                    if (err) return cb(err);

                    var cfg = JSON.parse(stdout);

                    self.cfgs[ho] = cfg;

                    cb();
                }
            );
        }
        else cb();
    },

    initWorker: function() {
        this.worker = app.context.socket('WORKER', { persistent: true, prefetch: 1 });
        this.worker.on('data', this.task.bind(this));

        this.worker.connect('parse', function() {
            log.verbose('parse workers connected');
        });
    },

    task: function(task) {
        var self = this;
        try {
            task = JSON.parse(task);

            this.loadCfg(task.ho, function(err) {
                if (err) {
                    log.error('parse: ' + err);
                    return self.worker.discard();
                }
                self.parse(task, function(err) {
                    if (err) {
                        log.error('parse: ' + err);
                        return self.worker.discard();
                        /*return this.worker.requeue();*/
                    }

                    self.worker.ack();
                });
            });
        }
        catch (e) {
            this.worker.discard();
        }
    },

    init: function() {
        this.initRegex();
        this.initWorker();

    }
};
