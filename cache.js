var app     = require("./app"),
    crypto  = require("crypto"),
    path    = require("path"),
    zlib    = require('zlib'),
    fs      = require("file-system"),
    /*msgpack = require("msgpack-lite"),*/
util    = require('./util'),
    async   = require('async'),
    _       = require('lodash'),
    log     = app.logger
;

module.exports = {

    save: function(task, cb) {
        var self = this;

        task = JSON.parse(task);

        var hash = crypto.createHash("md5").update(task.item.url).digest("hex");
        var p    = hash[0] + '/' + hash[0] + hash[1] + '/';
        var dir  = path.join(__dirname, 'data', task.ho, 'cache', p);
        var file = dir + hash + '.cache';
console.log(task.item.url);
console.log(hash);
console.log(file);
        async.waterfall([
            function(cb)                       { util.mkdir(dir, cb); },
            function(cb)                       { self.compress(task, cb); },
            function(content, format, enc, cb) { self.compose(task, content, format, enc, cb); },

            // in binary enc all utf8 chars will be cracked
            function(data, format, enc, cb)            { self.write(file, data, format, enc, cb); }
        ], function(err, length, format, enc) {
            if (err) return cb(err);

            log.info('%d\t%s\t%s\t%s\t\t%s', task.statusCode, task.html.length + '/' + length, task.mime + (task.charset ? '/' + task.charset : ''), 'cache/' + format + '/' + enc, task.item.url);

            app.tasker.post('parse', JSON.stringify({ item: task.item, file: file, ho: task.ho, col: task.col }), 'utf8');
            cb();
        });
    },

    write: function(file, data, format, enc, cb) {
        util.write(file, data, enc, function(err, length) {
            cb(err, length, format, enc);
        });
    },

    compose: function(task, content, format, enc, cb) {
        var h = this.composeHeaders(task.h);

        var data = [
            Math.floor(Date.now() / 1000),
            task.url,
            h.join("\r\n"),
            format,
            content.toString(enc) // task.html
        ];

        /*var da= [
          Math.floor(Date.now() / 1000),
          task.url,
          h.join("\r\n"),
          format,
          content // task.html
          ];

          var writeStream = fs.createWriteStream(file + '.pack');
          var encodeStream = msgpack.createEncodeStream();
          encodeStream.pipe(writeStream);
          encodeStream.write(da);*/

        /*data = JSON.stringify(data);*/
        data = data.join("\r\n\r\n");
        //data += "\r\n\r\n" + gzipped.toString('binary');
        cb(null, data, format, enc);
    },

    compress: function(task, cb) {
        //application/atom+xml
        //application/rss+xml
        //application/xhtml+xml
        //application/rdf+xml
        //application/javascript
        //application/x-javascript
        //application/ecmascript
        //application/json
        //application/x-web-app-manifest+json
        //application/vnd.ms-fontobject
        //application/x-font-ttf
        //application/xml
        //font/opentype
        //image/svg+xml
        //image/x-icon
        //image/bmp
        //text\/*
        var isGzip = task.mime.match(/text|javascript|json|ecmascript|atom\+xml|rss\+xml|xhtml\+xml|rdf\+xml|application\/xml|application\/x-font-ttf|font\.openfont|image\/svg\+xml|image\/x-icon|image\.bmp/) ? true : false;
        var buf = new Buffer(task.html, task.enc);
        if (isGzip && buf.length > 1024) {
            zlib.gzip(buf, function(err, gzipped) {
                cb(err, gzipped, 'gzip', 'binary');
            });
        }
        else cb(null, buf, 'raw', task.enc);
    },


    composeHeaders: function(headers) {
        var h = [];
        if (headers) {
            _.forEach(headers, function(el, idx) {
                if (Array.isArray(el)) {
                    _.forEach(el, function(el2) {
                        h.push(idx + ': ' + el2);
                    });
                }
                else {
                    h.push(idx + ': ' + el);
                }
            });
        }
        return h;
    },

    init: function() {
        var worker = app.context.socket('WORKER', { persistent: true, prefetch: 1 });
        worker.on('data', function(task) {
            this.save(task, function(err) {
                if (err) {
                    log.error('cache: ' + err);
                    return worker.requeue();
                }

                worker.ack();
            });
        }.bind(this));

        worker.connect('cache', function() {
            log.verbose('cache worker connected');
        });
    }
};
