var app        = require('./app'),
    uri        = require("urijs"),
    log        = app.logger
;

module.exports = {
    addStartUrl: function(project, cb) {
        log.warn('project ' + project.url + ' add start url');

        project.url = uri(project.url).normalize().toString();
        app.db.collection(project.col).save({ u: project.url, s: 'n', d: new Date() }, function(err, saved) {
            if (err) return cb('project ' + project.url + ' add start url err: ' + err);
            return cb();
        });
    },

    finish: function(project, cb) {
        app.db.collection('projects').update({ _id: project._id }, { $set: { s: 'f' } }, function(err, updated) {
            if (err) return cb('project ' + project.url + ' err: ' + err);

            log.warn('project ' + project.url + ' finished');
            cb();
        });
    },

    generateColHost: function(project, cb) {
        if (project.col && project.h) return cb(null, project);

        var host = uri(project.url).hostname().replace('www.', '');
        var col  = host.replace('.', '');

        project.h   = host;
        project.col = col;

        app.db.collection('projects').update({ _id: project._id }, { $set: { h: host, col: col } }, function(err, updated) {
            if (err) return cb('project ' + project.url + ' col update err: ' + err);

            cb(null, project);
        });
    },

    ensureIndex: function(project, cb) {
                // ensure index nad process project
                app.db.collection(project.col).ensureIndex({ u: 1, s: 1 }, function(err) {
                    if (err) return cb('project ' + project.col + ' ensure index err: ' + err);
                    cb(null, project);
                });
    }
};
