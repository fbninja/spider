var app     = require("./app"),
    crypto  = require("crypto"),
    path    = require("path"),
    zlib    = require('zlib'),
    util    = require('./util'),
    async   = require('async'),
    _       = require('lodash'),
    log     = app.logger
;

module.exports = {

    save: function(task, cb) {
        var self = this;

        task = JSON.parse(task);

        var p    = _.trimEnd(task.item.path, '/').split('/');
        var fn   = p.pop();
        var dir  = path.join(__dirname, 'data', task.ho, 'static', p.join('/'));
        var file = dir + '/' + fn;

        async.waterfall([
            function(cb) { util.mkdir(dir, cb); },
            function(cb) {
                if (!util.isFile(file)) {
                    log.silly('static: ' + file + ' is directory, can not save. Requested ' + task.item.path);
                    return cb();
                }

            util.write(file, task.html, task.enc, cb);
            }
        ], function(err, length) {
            if (err) return cb(err);

            log.info('%d\t%d\t%s\t%s\t%s\t%s', task.statusCode, length, task.h['content-type'], task.enc, 'static', task.item.url);
            cb();
        });
    },


    init: function() {
        var worker = app.context.socket('WORKER', { persistent: true, prefetch: 1 });
        worker.on('data', function(task) {
            this.save(task, function(err) {
                if (err) {
                    log.error('static: ' + err);
                    task = JSON.parse(task);
                    console.log(task);
                    process.exit();
                    return worker.requeue();
                }

                worker.ack();
            });
        }.bind(this));

        worker.connect('static', function() {
            log.verbose('static worker connected');
        });
    }
};
